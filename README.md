<!--
 * @Author: wangDeJiang(aei)
 * @Date: 2022-01-28 17:08:59
 * @LastEditors: wangDeJiang(aei)
 * @LastEditTime: 2022-03-10 15:53:47
 * @Description: file content
-->

# wx-stomp

## git
> https://gitee.com/wizardAEI/wx-stomp (欢迎加入开源项目，一起完善npm包！)

## 使用方法
``` js
import Ws from 'wx-stomp'

/**
* @description new一个ws实例，最好放在data里 url为ws地址如ws://xxx.com header为头部信息
*/
this.setData({
	ws: new Ws(url, header, options)
})

/**
* @description websocket连接 支持promise风格  res.statusCode == 1 说明连接成功
*/
const res = await this.data.ws.connect() 

/**
* @description websocket订阅 destination为订阅地址如/data/sub callback为订阅后广播发送过来的回调, optionns为其他配置，现在包含了duration及操作
options的值可以为:

	{
		duration: 12000,
		timeOutCallback: () => {
			console.log('超过12s，延时啦')
			//一系列操作
		}
	}
*/
this.data.ws.subscribe(destination, callback, header, options)


/**
 * @description websocket发送 url为发送地址，header为头部信息，data为发送的数据
 */
this.data.wssend(url, header, data) 
//例如如下发送形式
this.data.ws.send('/get/handle', {}, {
	num: 1
})

/**
 * @description 取消指定的订阅,注意sub最好还是存在data中，防止因为函数生命周期产生的意外。注意这里取消订阅后sub并不会变为null
 */
const sub = this.data.ws.subscribe(destination, callback, header, options) //每个订阅都会产生一个独立的sub
this.data.ws.unsubscribe(sub)

/**
* @description 断开连接 url为ws地址如ws://xxx.co
*/
this.data.ws.close(url)

```

## 使用技巧

### data中的ws不会显示类型

由于wx的data中数据再进行赋值(即setData)的时候，并不会主动改变数据的类型，例如：

```js
	data: {
		ws: null
	}
	
	...
	function fn() {
		this.setData({
			ws: new Ws(config.wsUrl, {})
		})
	}	
```

此时，ws的类型仍是null。这种情况下，当我们使用库中函数的时候，就会由于类型没有自动提示。

我们可以利用JSdoc的类型注解来解决这个问题。
```js
	data: {
		/**
     	* @type {Ws | null}
     	*/
		ws: null
	}
	
	...
	function fn() {
		this.setData({
			ws: new Ws(config.wsUrl, {})
		})
	}
```

### 判断订阅超时

如果订阅后长时间没收到消息，如何自主判断订阅超时？

1. 我们可以利用订阅的回调来判断：

```js
	page({
		data: {
			timeout: false
		}
		....
		,
		fn() {
			const sub = this.data.ws.subscribe(`/lala`, (msg) => {
				//如果收到了订阅的回调，则判断没有超时
				this.setData({
					timeout: false
				})
				...
			})

			//暂时置为超时
			this.setData({
				timeout: true
			})

			//核心操作，等待15s后查看是否超时标识仍为true，如果是，我们手动判断订阅超时，并且结束订阅，并进行一系列操作
			setTimeout(() => {
				if(this.data.subTimeOut) {
					console.log('超时')
					this.setData({
					timeout: false
					})
					his.data.ws.unsubscribe(this.data.sub) //取消订阅
					...
				}
			}, 15000)

			this.data.ws.send('/he', {}, {
				...
			})
		}
	})
```

2. 也可以使用1.1.2及以后的版本的新参数来指定等待时长

```js
	{
		duration: 12000,
		timeOutCallback: () => {
			console.log('超过12s，延时啦')
			//一系列操作
		}
	}
```