/*
 * @Descripttion: 
 * @Author: Wang Dejiang(aei)
 * @Date: 2022-01-29 15:03:16
 * @LastEditors: wangDeJiang(aei)
 * @LastEditTime: 2022-03-10 16:07:54
 */
export default class Ws {
    constructor(url: any, header: any, options: ?{
        socketTaskErrorCallback: Function,
        socketTaskCloseCallback: Function
    });
    url: any;
    header: any;
    socketTask: any;
    stompClient: any;
    msgArr: any[];
    isClose: boolean;
    ws: {
        send: (frame: any) => void;
        close: (frame: any) => void;
    };
    connect(): Promise<unknown>;
    getSocketStatus(): boolean;
    subscribe(url: string, fn: Function, header: object, options: ?{
        duration: number,
        timeOutCallback: Function
    }): any;
    unsubscribe(sub: any): boolean;
    send(url: string, header: any, data: any): void;
    close(url: string): void;
    setUrl(url: string): void;
}
