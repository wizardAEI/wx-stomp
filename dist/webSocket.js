'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); 

var _stomp = require('./stomp');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Ws = function () {
  function Ws(url, header, options) {
    var _this = this;

    _classCallCheck(this, Ws);

    this.url = null;
    this.header = null;
    this.socketTask = null;
    this.stompClient = null;
    this.msgArr = [];
    this.isClose = false;
    this.socketTaskErrorCallback = null;
    this.ws = {
      send: function send(frame) {
        if (_this.getSocketStatus()) {
          _this.socketTask.send({
            data: JSON.stringify([frame]),
            success: function success(res) {
              // console.log('sendBack: ',res)
            },
            fail: function fail(err) {
              console.error('sendBackERR: ', err);
            }
          });
        } else {
          _this.msgArr.push(frame);
        }
      },
      close: function close(frame) {
        // console.log('close')
        _this.socketTask.close(frame);
      }
    };

    this.url = url;
    this.header = header;
    this.socketTaskErrorCallback = options.socketTaskErrorCallback ? null : options.socketTaskErrorCallback;
  }

  _createClass(Ws, [{
    key: 'connect',
    value: function () {
      var ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee() {
        var _this2 = this;

        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.isClose = false;
                return _context.abrupt('return', new Promise(function (resovle, reject) {
                  _this2.socketTask = wx.connectSocket({
                    url: _this2.url,
                    header: _this2.header,
                    success: function success() {
                      _this2.stompClient = _stomp.Stomp.over(_this2.ws);
                      _this2.stompClient.connect({}, function () {
                        resovle({
                          statusCode: 1,
                          msg: '连接成功'
                        });
                      }, function () {
                        reject({
                          statusCode: 2,
                          msg: '连接失败'
                        });
                      });
                    }
                  });
                  _this2.socketTask.onOpen(function () {
                    // console.log('开始连接')
                    _this2.ws.onopen();
                    _this2.msgArr.forEach(function (val) {
                      _this2.ws.send(val);
                    });
                    _this2.msgArr = [];
                  });
                  _this2.socketTask.onMessage(function (frame) {
                    if (frame.data.indexOf('[') == -1) return;
                    var str = frame.data.slice(frame.data.indexOf('['));
                    if (str.length > 0) {
                      str = '' + JSON.parse(str);
                      _this2.ws.onmessage({
                        data: str
                      });
                    }
                  });
                  _this2.socketTask.onError(function (err) {
                    console.log('err', err);
                    _this2.socketTaskErrorCallback && _this2.socketTaskErrorCallback();
                    var error = new Error({
                      errMsg: err.errMsg
                    });
                    return error;
                  });
                  _this2.socketTask.onClose(function (res) {
                    // console.log('close', res)
                    _this2.stompClient._cleanUp();
                    if (!_this2.isClose) setTimeout(function () {
                      _this2.connect();
                    }, 1000);
                  });
                }));

              case 2:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function connect() {
        return ref.apply(this, arguments);
      }

      return connect;
    }()
  }, {
    key: 'getSocketStatus',
    value: function getSocketStatus() {
      var boolean = false;
      if (this.socketTask && this.socketTask.readyState) {
        boolean = this.socketTask.readyState === 1;
      }
      return boolean;
    }
  }, {
    key: 'subscribe',
    value: function subscribe(url, fn, header) {
      var _this3 = this;

      var options = arguments.length <= 3 || arguments[3] === undefined ? {} : arguments[3];

      if (Object.keys(options).length === 0) {
        var sub = this.stompClient.subscribe(url, fn, header);
        return sub;
      } else {
        if (options.duration && options.timeOutCallback) {
          var _ret = function () {
            var timer = setTimeout(function () {
              options.timeOutCallback();
            }, options.duration);
            var sub = _this3.stompClient.subscribe(url, function (msg) {
              clearTimeout(timer);
              fn(msg);
            }, header);
            return {
              v: sub
            };
          }();

          if ((typeof _ret === 'undefined' ? 'undefined' : _typeof(_ret)) === "object") return _ret.v;
        }
      }
    }
  }, {
    key: 'unsubscribe',
    value: function unsubscribe(sub) {
      if (sub) sub.unsubscribe();
    }
  }, {
    key: 'send',
    value: function send(url, header, data) {
      data = JSON.stringify(data);
      this.stompClient.send(url, header, data);
    }
  }, {
    key: 'close',
    value: function close(url) {
      var _this4 = this;

      if (this.stompClient && !this.isClose) this.stompClient.disconnect(function () {
        if (url == _this4.url) {
          _this4.isClose = true;
          _this4.socketTask.close();
        }
      });else {
        this.isClose = true;
      }
    }
  }]);

  return Ws;
}();

exports.default = Ws;