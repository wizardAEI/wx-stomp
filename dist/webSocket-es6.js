/*
 * @Author: wangDeJiang(aei)
 * @Date: 2022-01-28 17:30:06
 * @LastEditors: wangDeJiang(aei)
 * @LastEditTime: 2022-03-10 16:07:35
 * @Description: file content
 */
import { Stomp } from './stomp'
export default class Ws {
  constructor(url, header, options) {
    this.url = url
    this.header = header
    this.socketTaskErrorCallback = options.socketTaskErrorCallback ? options.socketTaskErrorCallback : null
    this.socketTaskCloseCallback = options.socketTaskCloseCallback ? options.socketTaskCloseCallback : null
  }
  url = null
  header = null
  socketTask = null
  stompClient = null
  msgArr = []
  isClose = false
  socketTaskErrorCallback = null
  socketTaskCloseCallback = null
  ws = {
    send: (frame) => {
      if(this.getSocketStatus()) {
        this.socketTask.send({ 
          data: JSON.stringify([frame]),
          success: (res) => {
            // console.log('sendBack: ',res)
          },
          fail: (err) => {
            console.error('sendBackERR: ',err)
          } 
        });
      }else {
        this.msgArr.push(frame)
      }
    },
    close: (frame) => {
      // console.log('close')
      this.socketTask.close(frame);
    }
  }
  async connect() {
    this.isClose = false
    return new Promise((resovle, reject) => {
      this.socketTask =  wx.connectSocket({
        url: this.url,
        header: this.header,
        success: () => {  
          this.stompClient = Stomp.over(this.ws)
          this.stompClient.connect({},() => {
            resovle({
              statusCode: 1,
              msg: '连接成功'
            })
          },() => {
            reject({
              statusCode: 2,
              msg: '连接失败'
            })
          })
        }
      })
      this.socketTask.onOpen(() => {
        // console.log('开始连接')
        this.ws.onopen();
        this.msgArr.forEach(val => {
          this.ws.send(val)      
        })
        this.msgArr = []
      })
      this.socketTask.onMessage((frame) => {
        if(frame.data.indexOf('[') == -1) return
        let str = frame.data.slice(frame.data.indexOf('['))
        if(str.length > 0) {
          str = `${JSON.parse(str)}`
          this.ws.onmessage({
            data: str
          })
        }
      })
      this.socketTask.onError((err) => {
        console.log('err', err)
        this.socketTaskErrorCallback && this.socketTaskErrorCallback()
        const error = new Error({
          errMsg: err.errMsg
        })
        return error
      })
      this.socketTask.onClose((res) => {
        // console.log('close', res)
        this.stompClient._cleanUp()
        this.socketTaskCloseCallback && this.socketTaskCloseCallback()
        if(!this.isClose)
        setTimeout(() => {
          this.connect()
        }, 1000);
      })
    })
  }
  getSocketStatus() {
    var boolean = false;
    if (this.socketTask && this.socketTask.readyState) {
      boolean = this.socketTask.readyState === 1;
    }
    return boolean;
  }
  subscribe(url, fn, header, options = {}) {
    if(Object.keys(options).length === 0) {
      let sub = this.stompClient.subscribe(url, fn, header)
      return sub
    }else {
      if(options.duration && options.timeOutCallback) {
        let timer = setTimeout(() => {
          options.timeOutCallback()
        }, options.duration)
        let sub = this.stompClient.subscribe(url, (msg) => {
          clearTimeout(timer)
          fn(msg)
        }, header)
        return sub
      }
    }
  }
  unsubscribe(sub) {
    if(sub)
    sub.unsubscribe()
  }
  send(url, header, data) {
    data = JSON.stringify(data)
    this.stompClient.send(url, header, data)
  }
  close(url) {
    if(this.stompClient && !this.isClose)
    this.stompClient.disconnect(() => {
      if(url == this.url) {
        this.isClose = true
        this.socketTask.close()
      }
    })
    else {
      this.isClose = true
    }
  }
  setUrl(url) {
    this.url = url
  }
}